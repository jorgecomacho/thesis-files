#!/usr/bin/env python

__author__ = 'John Kalin'

"""control.py: This program provides a very simply GUI to allow users to 
               change color and power of LIFX bulbs """

from Tkinter import *

import lifx_packet
import lifx_bulb
from socket import *
import threading
import colorsys

# Makes a GUI window
root = Tk()
root.wm_title("Simple Controller")

# Initializes UDP socket
UDP_IP = "0.0.0.0"
UDP_PORT = 56700

sock = socket(AF_INET, SOCK_DGRAM)   # INTERNET, UDP
sock.bind((UDP_IP, UDP_PORT))
sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
sock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

# Dictionary with ip as key
bulbs = {}

# List of known bulbs to verify discovery
def state_service(source, target):
    global bulbs
    if source not in bulbs.keys():
        # Placeholder Values
        temp = lifx_bulb.LifxBulb('name')
        bulbs[source]=temp
        bulbs[source].mac_address = target
    print "found", len(bulbs), "bulbs", source, bulbs[source].mac_address

# Listener socket to get replies
def listen_sock():
    while True:
        data, addr = sock.recvfrom(1024) # buffer size is 1024 byte
        response = lifx_packet.LifxPacket()        
        payload = response.unpack_header(data)
        args = response.dissect_payload(payload)
        # Listens for StateService messages
        if response.protocol_type == 3:
            state_service(addr[0], response.frame_address_target)

# Sends packet
def send_sock(target, data):
    sock.sendto(data, (target, UDP_PORT))

# sets to broadcast
tgt_addr = '255.255.255.255'

rgb = "#%02x%02x%02x" % (0, 0, 0)
#Function to get RGB value
def updateValue(self):
    global rgb
    red2 = r_scale.get()
    green2 = g_scale.get()
    blue2 = b_scale.get()
    h, s, v = colorsys.rgb_to_hsv(red2/255.0, green2/255.0, blue2/255.0)
    rgb = "#%02x%02x%02x" % (red2, green2, blue2)
    # Updates background to show current color
    color_window.configure(bg = rgb)

    # Sends a set color message to the bulb
    send_packet = lifx_packet.LifxPacket()
    send_packet.frame_source = 5203
    temp = send_packet.pack_send(0, 1, "SetColor", 0, h*65535, s*65535, v*65535, 9000, 0)
    send_sock(tgt_addr, temp)

# Sends turn on message
def turn_on():
    send_packet = lifx_packet.LifxPacket()
    send_packet.frame_source = 5203
    temp = send_packet.pack_send(0, 1, "SetPower", 65535)
    send_sock(tgt_addr, temp)

# Sends turn off message
def turn_off():
    send_packet = lifx_packet.LifxPacket()
    send_packet.frame_source = 5203
    temp = send_packet.pack_send(0, 1, "SetPower", 0)
    send_sock(tgt_addr, temp)

# Sends GetService message to look for new bulbs
def scan():
    send_packet = lifx_packet.LifxPacket()
    send_packet.frame_tagged = 1
    send_packet.frame_source = 5203
    temp = send_packet.pack_send(0, 1, "GetService", )
    send_sock(tgt_addr, temp)
    
# Color labels
red = Label (root, text='R', font='Calibri', fg='red')
green = Label (root, text='G', font='Calibri', fg='green')
blue = Label (root, text='B', font='Calibri', fg='blue')

on = Button (root, text='ON', font='Calibri', command=turn_on)
off = Button (root, text='OFF', font='Calibri', command=turn_off)
scan = Button (root, text='Scan', font='Calibri', command=scan)

# Canvas window
color_window = Canvas(root, width=300, height=100, borderwidth=5, relief='raised', bg=rgb)

# Scale colors
r_scale = Scale(root, from_=0, to=255, length=300, sliderlength=60, orient=HORIZONTAL,command=updateValue)
g_scale = Scale(root, from_=0, to=255, length=300, sliderlength=60, orient=HORIZONTAL,command=updateValue)
b_scale = Scale(root, from_=0, to=255, length=300, sliderlength=60, orient=HORIZONTAL,command=updateValue)
   
# Position of labels and buttons
red.grid (row=1, column=0, sticky=W)
green.grid (row=2, column=0, sticky=W)
blue.grid (row=3, column=0, sticky=W)

on.grid (row=0, column=0)
off.grid (row=0, column=1)
scan.grid (row=0, column=2)

r_scale.grid (row=1, column=1)
g_scale.grid (row=2, column=1)
b_scale.grid (row=3, column=1)

color_window.grid(row=4, column=1)

# Setwindow size
root.resizable(width=FALSE, height=FALSE)
root.minsize(width=425, height=250)
root.maxsize(width=425, height=250)

# Threads listener to not block event loop
t = threading.Thread(target=listen_sock,args=())
t.start()

mainloop()
