#!/usr/bin/env python

__author__ = 'John Kalin'

"""bulb_sim.py: This program simulates an instance of a LIFX bulb, including
                a visual display of the light and traffic sent and received"""

from Tkinter import *
from PIL import ImageTk
import time
import threading
import lifx_packet as l
import lifx_bulb
from socket import *
import colorsys
import os

# Generates GUI window
root = Tk()
root.title("LIFX Bulb")

# Subdivides the window into a frame
frame = Frame(root)
frame.pack(side=LEFT)

# Holds RGB value for bulb color
r = 255
g = 255
b = 255
# RGB hex string
st = "#%02x%02x%02x" % (r, g, b)

# Splits display to show image of bulb on left, background color is variable
canvas = Canvas(frame, bg=st, width=500, height=500)
canvas.pack(side=LEFT)
photoimage = ImageTk.PhotoImage(file="bulb3.png")
canvas.create_image(250, 250, image=photoimage)

# Adds a second frame on right for text
frame2 = Frame(root)
frame2.pack(side=RIGHT)

# Configures the right half of the display to have scrolling text
text2 = Text(frame2, height=35, width=40)
scroll = Scrollbar(frame2, command=text2.yview)
text2.configure(yscrollcommand=scroll.set)
text2.tag_configure('bold_italics', font=('Arial', 12, 'bold', 'italic'))
text2.tag_configure('big', font=('Arial', 16, 'bold'))
text2.tag_configure('receive', foreground='#209042', 
						font=('Arial', 12, 'bold'))
text2.tag_configure('send', foreground='#902042', 
						font=('Arial', 12, 'bold'))
text2.tag_configure('standard', foreground='black', 
						font=('Arial', 10))
text2.tag_bind('follow', '<1>', lambda e, 
               t=text2: t.insert(END, "Not now, maybe later!"))
text2.pack(side=LEFT)
scroll.pack(side=RIGHT, fill=Y)

# Network setup
UDP_IP = "0.0.0.0"
UDP_PORT = 56700    # LIFX port

# Sets up UDP socket
sock = socket(AF_INET, SOCK_DGRAM)   # INTERNET, UDP
sock.bind((UDP_IP, UDP_PORT))
sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
sock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

# Instantiates a lifx bulb
my_bulb = lifx_bulb.LifxBulb('NEWNAME')

# Used to prevent listener from reporting sent data as received
last_sent = ''

# Prints summary of important fields of packet on right of display
def print_pack(send, data):
    global text2

    # Makes an instance of a packet, and dissects incoming data into components
    packet = l.LifxPacket()
    payload = packet.unpack_header(data)
    args = packet.dissect_payload(payload)

    # Changes color based on sending or receiving
    if send:
        protocol = '\nSEND: ' + l.PROT_NAME[packet.protocol_type] + '\n'
        text2.insert(END, protocol, 'send')
    else:
        protocol = '\nRECEIVE: ' + l.PROT_NAME[packet.protocol_type] + '\n'
        text2.insert(END, protocol, 'receive')

    # Prints important fiels, as well as a hex dump of the whole packet
    text2.insert(END, 'Size:\t%d\n' % packet.frame_size, 'standard')
    text2.insert(END, 'Origin:\t%d\n' % packet.frame_origin, 'standard')
    text2.insert(END, 'Tagged:\t%d\n' % packet.frame_tagged, 'standard')
    text2.insert(END, 'Addressable:\t%d\n' % packet.frame_addressable, 'standard')
    text2.insert(END, 'Protocol:\t%d\n' % packet.frame_protocol, 'standard')
    text2.insert(END, 'Source:\t%d\n' % packet.frame_source, 'standard')
    text2.insert(END, 'Target:\t%s\n' % packet.frame_address_target, 'standard')
    text2.insert(END, 'Ack Req:\t%d\n' % packet.frame_address_ack_required, 'standard')
    text2.insert(END, 'Response Req:\t%d\n' % packet.frame_address_res_required, 'standard')
    text2.insert(END, 'Sequence:\t%d\n' % packet.frame_address_sequence, 'standard')
    text2.insert(END, 'Protocol Type:\t%d\n' % packet.protocol_type, 'standard')
    text2.insert(END, 'Payload length:\t%d\n' % len(args), 'standard')
    for i in range(len(args)):
        try:
            text2.insert(END, 'Payload %d:\t%s\n' % (i, args[i]), 'standard')
        except:
            text2.insert(END, 'Payload %d:\t%d\n' % (i, args[i]), 'standard')
    
    text2.insert(END, 'Hex:[', 'standard')
    for c in data:
        text2.insert(END, '%02x,' % ord(c), 'standard')
    text2.insert(END, ']\n', 'standard')
    text2.see(END)
    
# Listens for incoming UDP packets to port 56700
def listen_sock():
    global st
    last_sent = ''
    while True:
        data, addr = sock.recvfrom(1024) # buffer size is 1024 byte
        # Keeps listening until packet is not the one that was just sent out
        while data == last_sent:
            data, addr = sock.recvfrom(1024) # buffer size is 1024 byte

        # Prints out received packet
        print_pack(False, data) 

        # Instantiates a LIFX packet for reply
        response = l.LifxPacket()
        payload = response.unpack_header(data)

        # Determines appropriate reply
        reply = my_bulb.interpret_action(data)
        
        # Convers HSVB tor RGB for display
        h = my_bulb.color_hue
        s = my_bulb.color_saturation
        b = my_bulb.color_brightness
        r1, g1, b1 = colorsys.hsv_to_rgb(h/65535.0, s/65535.0, b/65535.0)
        r, g, b = int(r1*255), int(g1*255), int(b1*255)
        st = "#%02x%02x%02x" % (r, g, b)

        # Sets background to white when bulb off
        if my_bulb.level == 0:
            st = "#ffffff"

        # Repaints background
        canvas['bg'] = st
        
        # Sends all reply packets
        for pack in reply:
            send_sock(addr[0], addr[1], pack)
            last_sent = pack

# Sends replies and prints packets
def send_sock(target, port_num, data):
    sock.sendto(data, (target, port_num))
    print_pack(True, data)

# Threads listener to accomodate GUI's event loop
t = threading.Thread(target=listen_sock,args=())
t.start()

root.mainloop()

# Exits all threads when GUI closed
os._exit(0)
