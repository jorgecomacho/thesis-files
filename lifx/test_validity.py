#!/usr/bin/env python

__author__ = 'John Kalin'

"""test_validity.py: send all valid commands and log replies in text file, used
                     for validating simulation against actual bulb"""

import lifx_packet
import lifx_bulb
from socket import *
import threading
import time

UDP_IP = "0.0.0.0"
UDP_PORT = 56700

sock = socket(AF_INET, SOCK_DGRAM)   # INTERNET, UDP
sock.bind((UDP_IP, UDP_PORT))
sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
sock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

# Lists of different values to try combinations of
ORIGIN = [0, 1]
TAGGED = [0, 1]
ADDRESS = [0, 1]
ACK = [0, 1]
RES = [0, 1]
ID = ['000000000000', 'd073d511d23d']

# All protocols with associates actions or replies
TYPE = ["GetService", "GetHostInfo", "GetHostFirmware", "GetWifiInfo", 
        "GetWifiFirmware", "GetPower", "GetLabel", "GetVersion", 
        "GetInfo", "Ack", "GetLocation", "GetGroup", "Get", "GetPowerLight", 
        "GetInfrared"]

# Text file to be written
filename = open('log_bulb', 'w')

# A listener socket to send incoming messages to text file
def listen_sock():
    while True:
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        filename.write("\nReceived from %s " % addr[0])
        # Makes a LIFX packet
        response = lifx_packet.LifxPacket()
        payload = response.unpack_header(data)
        
        # Looks up protocol name
        try:
            filename.write(" %s\n" % \
                           lifx_packet.PROT_NAME[response.protocol_type])
        except:
            filename.write(" unknown\n")

        # Sends important fields to text file
        filename.write("frame size: %d\n" % response.frame_size)
        filename.write("frame origin: %d\n" % response.frame_origin)
        filename.write("frame tagged: %d\n" % response.frame_tagged)
        filename.write("frame addressable: %d\n" % response.frame_addressable)
        filename.write("frame protocol: %d\n" % response.frame_protocol)
        filename.write("frame source: %d\n" % response.frame_source)
        filename.write("frame address target: %s\n" % \
                      response.frame_address_target)
        filename.write("frame address ack required: %d\n" % \
                      response.frame_address_ack_required)
        filename.write("frame address res required: %d\n" % \
                      response.frame_address_res_required)
        filename.write("frame address sequence: %d\n" % \
                      response.frame_address_sequence)
        filename.write("protocol type: %d\n" % response.protocol_type)
        
        # Prints arguments to file, no analysis
        args = response.dissect_payload(payload)
        filename.write("payload length: %d\n" % len(args))
        for i in args:
            try:
                filename.write("%s\n" % i)
            except:
                filename.write("%d\n" % i)

# Sends packets
def send_sock(target, data):
    sock.sendto(data, (target, UDP_PORT))

if __name__ == "__main__":

    # Threads listener to not block
    t = threading.Thread(target=listen_sock,args=())
    t.start()

    while True:
      # Sends every combination of varied fields
      try:   
        for a in ORIGIN:
          for b in TAGGED:
            for c in ADDRESS:
              for d in ACK:
                for e in RES: 
                  for g in ID:    
                    for f in TYPE:       
                      send_packet = lifx_packet.LifxPacket()
                      send_packet.frame_tagged = b
                      send_packet.frame_origin = a
                      send_packet.frame_addressable = c
                      send_packet.frame_source = 5203
                      send_packet.frame_address_target = g.decode("hex") + \
                                                         '\x00\x00'
                      temp = send_packet.pack_send(d, e, f,)
                      send_sock('255.255.255.255', temp)
                      time.sleep(0.25)

                    # Specialty packets that arent in master list due to format
                    # SetLabel
                    send_packet = lifx_packet.LifxPacket()
                    send_packet.frame_tagged = b
                    send_packet.frame_origin = a
                    send_packet.frame_addressable = c
                    send_packet.frame_source = 5203
                    send_packet.frame_address_target = g.decode("hex") + \
                                                       '\x00\x00'
                    temp = send_packet.pack_send(d, e, "SetLabel", ("NEWNAME",))
                    send_sock('255.255.255.255', temp)
                    time.sleep(0.25)

                    # EchoRequest
                    send_packet = lifx_packet.LifxPacket()
                    send_packet.frame_tagged = b
                    send_packet.frame_origin = a
                    send_packet.frame_addressable = c
                    send_packet.frame_source = 5203
                    send_packet.frame_address_target = g.decode("hex") + '\x00\x00'
                    temp = send_packet.pack_send(d, e, "EchoRequest", "echo me")
                    send_sock('255.255.255.255', temp)
                    time.sleep(0.25)

                    # SetPower
                    send_packet = lifx_packet.LifxPacket()
                    send_packet.frame_tagged = b
                    send_packet.frame_origin = a
                    send_packet.frame_addressable = c
                    send_packet.frame_source = 5203
                    send_packet.frame_address_target = g.decode("hex") + \
                                                      '\x00\x00'
                    temp = send_packet.pack_send(d, e, "SetPower", 65535)
                    send_sock('255.255.255.255', temp)
                    time.sleep(0.25)

                    # SetColor
                    send_packet = lifx_packet.LifxPacket()
                    send_packet.frame_tagged = b
                    send_packet.frame_origin = a
                    send_packet.frame_addressable = c
                    send_packet.frame_source = 5203
                    send_packet.frame_address_target = g.decode("hex") + \
                                                      '\x00\x00'
                    temp = send_packet.pack_send(d, e, "SetColor", 0, 30000, \
                                                30000, 30000, 9000, 0)
                    send_sock('255.255.255.255', temp)
                    time.sleep(0.25)

                    # SetPowerLight
                    send_packet = lifx_packet.LifxPacket()
                    send_packet.frame_tagged = b
                    send_packet.frame_origin = a
                    send_packet.frame_addressable = c
                    send_packet.frame_source = 5203
                    send_packet.frame_address_target = g.decode("hex") + \
                                                      '\x00\x00'
                    temp = send_packet.pack_send(d, e, "SetPowerLight", 0, 0)
                    send_sock('255.255.255.255', temp)
                    time.sleep(0.25)

                    # SetInfrared
                    send_packet = lifx_packet.LifxPacket()
                    send_packet.frame_tagged = b
                    send_packet.frame_origin = a
                    send_packet.frame_addressable = c
                    send_packet.frame_source = 5203
                    send_packet.frame_address_target = g.decode("hex") + \
                                                      '\x00\x00'
                    temp = send_packet.pack_send(d, e, "SetInfrared", 0)
                    send_sock('255.255.255.255', temp)
                    time.sleep(0.25)
      except:
        send_packet.print_packet()
        print "Something went wrong"
      
      # Waits for user input to close file
      raw_input('\ndone')

      filename.close()
