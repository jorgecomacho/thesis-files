#!/usr/bin/env python

__author__ = 'John Kalin'

"""testreceive.py: receive and format lifx traffic for user viewing
Author: John Kalin"""

import lifx_packet
import lifx_bulb
from socket import *
import threading

# UDP socket setup
UDP_IP = "0.0.0.0"
UDP_PORT = 56700

sock = socket(AF_INET, SOCK_DGRAM)   # INTERNET, UDP
sock.bind((UDP_IP, UDP_PORT))
sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
sock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

# Prints received packets
def listen_sock():
    while True:
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        print '\n\nRECEIVED: ', data, 'from', addr[0]
        response = lifx_packet.LifxPacket()
        payload = response.unpack_header(data)
        response.print_packet()   
        args = response.dissect_payload(payload)
        print response.protocol_type, 'payload', args

# Sends data
def send_sock(target, data):
    sock.sendto(data, (target, UDP_PORT))

if __name__ == "__main__":

    t = threading.Thread(target=listen_sock,args=())
    t.start()

    while True:
        command = raw_input("Enter Command: ")

        send_packet = lifx_packet.LifxPacket()

        # Can add or modify commands as needed for testing individual 
        # instructions
        if command == "discover":
            send_packet.frame_tagged = 0
            send_packet.frame_source = 5203
            temp = send_packet.pack_send(1, 0, "GetService", )
            send_sock('255.255.255.255', temp)
      
