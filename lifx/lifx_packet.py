#!/usr/bin/env python

__author__ = 'John Kalin'

"""lifx_packet.py: This class is an object representation of a LIFX packet"""

import struct

HEADER_SIZE = 36		# Bytes

# A dictionary with protocol names as the key, and struct formats of the payload
# as the value. Used for packing and unpacking binary data, see struct 
# documentation
PAYLOAD_FMT = {"GetService": "", "StateService": "=BI", "GetHostInfo": "", 
               "StateHostInfo": "IIIH", "GetHostFirmware": "", 
               "StateHostFirmware": "QQI", "GetWifiInfo": "", 
               "StateWifiInfo": "IIIH", "GetWifiFirmware": "", 
               "StateWifiFirmware": "QQI", "GetPower": "", "SetPower": "H",
               "StatePower": "H", "GetLabel": "", "GetVersion": "", 
               "StateVersion": "III", "GetInfo": "", "StateInfo": "QQQ", 
               "Ack": "", "GetLocation": "", "GetGroup": "", "Get": "",
               "SetColor": "=BHHHHI", "GetPowerLight": "", 
               "SetPowerLight": "=HI", "StatePowerLight": "H", 
               "GetInfrared": "", "StateInfrared": "H", "SetInfrared": "H"}
               # missing SetLabel, StateLabel, StateLocation, StateGroup, 
               # EchoRequest, EchoResponse, State
               # These protocols cannot be cleanly packed using struct 

# Dictionary to translate protocol number to protocol name
PROT_NAME = {2: "GetService", 3: "StateService", 12: "GetHostInfo", 
             13: "StateHostInfo", 14: "GetHostFirmware", 
             15: "StateHostFirmware", 16: "GetWifiInfo", 17: "StateWifiInfo",
             18: "GetWifiFirmware", 19: "StateWifiFirmware", 20: "GetPower", 
             21: "SetPower", 22: "StatePower", 23: "GetLabel", 24: "SetLabel", 
             25: "StateLabel", 32: "GetVersion", 33: "StateVersion", 
             34: "GetInfo", 35: "StateInfo", 45: "Ack", 48: "GetLocation",
             51: "GetGroup", 101: "Get", 102: "SetColor", 116: "GetPowerLight", 
             117: "SetPowerLight", 118: "StatePowerLight", 120: "GetInfrared", 
             121: "StateInfrared", 122: "SetInfrared", 50: "StateLocation", 
             53: "StateGroup", 58: "EchoRequest", 59: "EchoResponse", 
             107: "State", 111: "Unknown", 406: "Unknown"}

# Dictionary to translate protocol name to protocol number
PROT_NUM = {"GetService": 2, "StateService": 3, "GetHostInfo": 12, 
            "StateHostInfo": 13, "GetHostFirmware": 14, "StateHostFirmware": 15,
            "GetWifiInfo": 16, "StateWifiInfo": 17, "GetWifiFirmware": 18,
            "StateWifiFirmware": 19, "GetPower": 20, "SetPower": 21, 
            "StatePower": 22, "GetLabel": 23, "SetLabel": 24, "StateLabel": 25, 
            "GetVersion": 32, "StateVersion": 33, "GetInfo": 34, 
            "StateInfo": 35, "Ack": 45, "GetLocation": 48, "GetGroup": 51, 
            "Get": 101, "SetColor": 102, "GetPowerLight": 116, 
            "SetPowerLight": 117, "StatePowerLight": 118, "GetInfrared": 120, 
            "StateInfrared": 121, "SetInfrared": 122, "StateLocation": 50, 
            "StateGroup": 53, "EchoRequest": 58, "EchoResponse": 59, 
            "State": 107}


class LifxPacket(object):

    def __init__(self):
        # frame fields
        self.frame_size = 0                   # 16-bit size of message
        self.frame_origin = 0                 # 2-bit, must be 0
        self.frame_tagged = 0                 # 1-bit, used with target
        self.frame_addressable = 1            # 1-bit, must be 1
        self.frame_protocol = 1024            # 12-bit, must be 1024
        self.frame_source = 0                 # 32-bit, used for response
        # frame address fields
        self.frame_address_target = '\x00'*8  # 64-bit, mac + 0's, or 0's
        self.frame_address_reserved = 'LIFXV2'# 48-bit, observed
        self.frame_address_reserved1 = 0      # 6-bit, unused
        self.frame_address_ack_required = 0   # 1-bit
        self.frame_address_res_required = 0   # 1-bit, response required
        self.frame_address_sequence = 0       # 8-bit, wrapped
        # protocol fields
        self.protocol_reserved = '\x00'*8     # 64-bit reserved
        self.protocol_type = 0                # 16-bit, determines payload
        self.protocol_reserved1 = 0           # 16-bit, reserved
        # payload fields not all used
        self.payload_brightness = 0
        self.payload_build = 0
        self.payload_color_brightness = 0
        self.payload_color_hue = 0
        self.payload_color_kelvin = 0
        self.payload_color_saturation = 0
        self.payload_downtime = 0
        self.payload_duration = 0
        self.payload_group = 0
        self.payload_label = 0
        self.payload_level = 0
        self.payload_location = 0
        self.payload_port = 0
        self.payload_power = 0
        self.payload_product = 0
        self.payload_reserved = 0
        self.payload_reserved1 = 0
        self.payload_rx = 0
        self.payload_service = 0
        self.payload_signal = 0
        self.payload_time = 0
        self.payload_tx = 0
        self.payload_updated_at = 0
        self.payload_uptime = 0
        self.payload_vendor = 0
        self.payload_version = 0
        self.payload_wifi_rx = 0
        self.payload_wifi_signal = 0
        self.payload_wifi_tx = 0

    # prints packet to terminal, used for diagnostics
    def print_packet(self):
        print "Frame fields:\nsize: ", self.frame_size
        print "origin: ", self.frame_origin
        print "tagged: ", self.frame_tagged
        print "addressable: ", self.frame_addressable
        print "protocol: ", self.frame_protocol
        print "source: ", self.frame_source, "\n"
        print "Frame Address fields:\ntarget: ", self.frame_address_target
        print "reserved: ", self.frame_address_reserved
        print "reserved1: ", self.frame_address_reserved1
        print "ack_required: ", self.frame_address_ack_required
        print "res_required: ", self.frame_address_res_required
        print "sequence: ", self.frame_address_sequence, "\n"
        print "Protocol fields:\nreserved: ", self.protocol_reserved
        print "type: ", self.protocol_type
        print "reserved1: ", self.protocol_reserved1, "\n"

    # Checks if commands are valid, based on observation, differs from spec
    def is_valid(self):
        if self.frame_protocol != 1024:
            return False
        if self.frame_addressable != 1:
            return False
        if self.frame_tagged == 1 and self.frame_address_target != '00'*6:
            return False
        return True

    # returns packet as a byte string
    def pack_send(self, ack, res, message_type, *args):
        self.frame_address_ack_required = ack
        self.frame_address_res_required = res
        self.protocol_type = PROT_NUM[message_type]

        # Attempts to resolve automatically
        try:
            payload = struct.pack(PAYLOAD_FMT[message_type], *args)
        # Handles exception packet types
        except:
            try:
                payload = struct.pack(PAYLOAD_FMT[message_type], *args[0])
            except:
                if message_type == "SetLabel" or message_type == "StateLabel":
                    # Pads to 32-byte string
                    payload = args[0][0] + (32-len(args[0][0]))*'\x00'
                elif message_type == "StateLocation" or \
                        message_type == "StateGroup":
                    # Pads to 16-bytes and 32-bytes
                    payload = args[0][0] + (16-len(args[0][0]))*'\x00'
                    payload += args[0][1] + (32-len(args[0][1]))*'\x00'
                    payload += struct.pack('Q', args[0][2])
                elif message_type == "EchoRequest" or \
                        message_type == "EchoResponse":
                    # More padding
                    payload = args[0][0] + (64-len(args[0][0]))*'\x00'
                elif message_type == "State":
                    payload = struct.pack('HHHHHH', args[0][0], args[0][1], \
                              args[0][2], args[0][3], args[0][4], args[0][5])
                    payload += args[0][6] + (32-len(args[0][6]))*'\x00'
                    payload += struct.pack('Q', args[0][7])
                else:
                    payload = ''

        # Calculates total size of lifx packet
        self.frame_size = HEADER_SIZE + len(payload)

        # Combines tag values into longer strings for more normal sizing
        tags = (self.frame_origin << 14) | (self.frame_tagged << 13) | \
               (self.frame_addressable << 12) | self.frame_protocol
        tags2 = (self.frame_address_ack_required << 1) | \
                self.frame_address_res_required

        # Builds header
        header = struct.pack("<HHI", self.frame_size, tags, self.frame_source) 
        header += self.frame_address_target
        header += self.frame_address_reserved
        header += struct.pack("BB", tags2, self.frame_address_sequence)
        header += self.protocol_reserved
        header += struct.pack("<HH", self.protocol_type, \
                             self.protocol_reserved1)

        return header + payload

    # Dissects a header into its component parts, returns payload
    def unpack_header(self, message):
        header_fmt = '=HHIBBBBBBHIHBBQHH'   # reflect data sizes, see struct doc

        # Splits head and payload section of packet        
        header = message[:HEADER_SIZE]
        payload = message[HEADER_SIZE:]
        header_unpacked = struct.unpack(header_fmt, header)

        # Frame fields
        self.frame_size = header_unpacked[0]
        self.frame_origin = header_unpacked[1] >> 14
        self.frame_tagged = (header_unpacked[1] >> 13) & 1
        self.frame_addressable = (header_unpacked[1] >> 12) & 1
        self.frame_protocol = header_unpacked[1] & 4095
        self.frame_source = header_unpacked[2]

        # Frame address fields
        self.frame_address_target = "%02x"*6 % (header_unpacked[3], 
                                                header_unpacked[4],
                                                header_unpacked[5], 
                                                header_unpacked[6],
                                                header_unpacked[7], 
                                                header_unpacked[8])
        self.frame_address_reserved = header_unpacked[10]
        self.frame_address_reserved1 = header_unpacked[12] >> 2
        self.frame_address_ack_required = (header_unpacked[12] >> 1) & 1
        self.frame_address_res_required = header_unpacked[12] & 1
        self.frame_address_sequence = header_unpacked[13]

        # Protocol fields
        self.protocol_reserved = header_unpacked[14]
        self.protocol_type = header_unpacked[15]
        self.protocol_reserved = header_unpacked[16]

        return payload

    # Should return a tuple of payload items
    def dissect_payload(self, payload):
        payload_args = ()

        # Tries to use dictionary to automatically generate tuple
        try:
            payload_fmt = PAYLOAD_FMT[PROT_NAME[self.protocol_type]]
            payload_args = struct.unpack(payload_fmt, payload)
        except:
            # SetLabel
            if self.protocol_type == 24:
                payload_args += (payload, )
            # StateLabel
            elif self.protocol_type == 25:
                payload_args += (payload, )
            # StateLocation or StateGroup
            elif self.protocol_type == 50 or self.protocol_type == 53:
                payload_args += (payload[0:16], )
                payload_args += (payload[16:48], )
                temp = struct.unpack('Q', payload[48:56])
                payload_args += (temp[0], )
            # EchoRequest or EchoResponse
            elif self.protocol_type == 58 or self.protocol_type == 59:
                payload_args += (payload, )
            # State
            elif self.protocol_type == 107:
                payload_format = 'HHHHHH'
                payload_args = struct.unpack(payload_format, payload[0:12])
                payload_args += (payload[12:44], )
                payload_args += (struct.unpack('Q', payload[44:52])[0], )
            # Mystery 406
            elif self.protocol_type == 406 or self.protocol_type == 111:
                payload_args += (payload, )
        return payload_args
