#!/usr/bin/env python

__author__ = 'John Kalin'

"""lifx_bulb.py: This class is an object representation of a simulated LIFX
                 Color 1000 smart light bulb"""

import lifx_packet as l
import struct
import time


# Constants 
UDP_PORT = 56700    # LIFX Port
SERVICE = 1         # UDP

VENDOR_ID = 1       # LIFX
PRODUCT_ID = 22     # Color 1000

# These are observed values from a physical bulb, they are information only
FIRMWARE_BUILD = 1444279139000000000
FIRMWARE_VERSION = 65543
WIFI_BUILD = 1435013262000000000
WIFI_VERSION = 6619162
HW_VERSION = 0
MCU_SIGNAL = 0          
WIFI_SIGNAL = 961624038


class LifxBulb(object):

    def __init__(self, label):
        # Initializes time to time of instantiation
        current_time = int(time.time() * 1000000000)

        # Identifiers
        self.location = '\x00'*16
        self.location_label = '\x00'*32
        self.location_updated_at = 0

        self.group = '\x00'*16
        self.group_label = '\x00'*32
        self.group_updated_at = 0

        # User-specified name of bulb
        self.label = label

        # Status
        self.level = 0
        self.color_hue = 0
        self.color_saturation = 0
        self.color_brightness = 0
        self.color_kelvin = 2500    # 2500 - 9000

        # Host Info
        self.tx = 0
        self.rx = 0
        self.on_time = current_time

        # Can be any MAC, but this format matches LIFX manufactured bulbs
        self.mac_address = "d073d511d23d"

    # Receives byte stream from network socket, and analyzes it
    def interpret_action(self, message):
        
        # Adds packet length to total transmitted bytes
        self.rx += len(message)

        # Creates an instance of a LIFX packet
        command = l.LifxPacket()
        # Gets payload from packet as a byte string
        command_payload = command.unpack_header(message)
        # Breaks payload into tuple of relevant data
        command_args = command.dissect_payload(command_payload)

        # Returns early if packet is invalid
        if not command.is_valid():
            return []

        # Determines if message applies to this bulb 
        tgt = command.frame_address_target
        if tgt != '00'*6 and tgt != self.mac_address:
            return []

        # Creates an instance of a LIFX packet for a response message
        reply_packet = l.LifxPacket()
        # Bulbs reply with their own MAC as the target
        reply_packet.frame_address_target = self.mac_address.decode("hex") + \
                                            '\x00\x00'
        # Reply should match sequence and source of command
        reply_packet.frame_address_sequence = command.frame_address_sequence
        reply_packet.frame_origin = 1
        reply_packet.frame_source = command.frame_source

        # Lists to hold replies, some messages may have multiple replies
        reply_payload = []
        reply_data = []

        # Builds reply based on message type
        # GetService
        if command.protocol_type == 2:
            reply_payload.append(SERVICE)
            reply_payload.append(UDP_PORT)
            reply_packet.protocol_type = 3
        # GetHostInfo
        elif command.protocol_type == 12:
            reply_payload.append(MCU_SIGNAL)
            reply_payload.append(self.tx)
            reply_payload.append(self.rx)
            reply_payload.append(0)
            reply_packet.protocol_type = 13
        # GetHostFirmware
        elif command.protocol_type == 14:
            reply_payload.append(FIRMWARE_BUILD)
            reply_payload.append(FIRMWARE_BUILD)
            reply_payload.append(FIRMWARE_VERSION)
            reply_packet.protocol_type = 15
        # GetWifiInfo
        elif command.protocol_type == 16:
            reply_payload.append(WIFI_SIGNAL)
            reply_payload.append(self.tx)
            reply_payload.append(self.rx)
            reply_payload.append(0)
            reply_packet.protocol_type = 17
        # GetWifiFirmware
        elif command.protocol_type == 18:
            reply_payload.append(WIFI_BUILD)
            reply_payload.append(0)
            reply_payload.append(WIFI_VERSION)
            reply_packet.protocol_type = 19
        # GetPower
        elif command.protocol_type == 20:
            reply_payload.append(self.level)
            reply_packet.protocol_type = 22
        # SetPower
        elif command.protocol_type == 21:
            self.level = command_args[0]
            if command.frame_address_res_required == 1:
                reply_payload.append(self.level)
                reply_packet.protocol_type = 22
        # GetLabel
        elif command.protocol_type == 23:
            reply_payload.append(self.label)
            reply_packet.protocol_type = 25
        # SetLabel
        elif command.protocol_type == 24:
            self.label = command_payload
            if command.frame_address_res_required == 1:
                reply_payload.append(self.label)
                reply_packet.protocol_type = 25
        # GetVersion
        elif command.protocol_type == 32:
            reply_payload.append(VENDOR_ID)
            reply_payload.append(PRODUCT_ID)
            reply_payload.append(HW_VERSION)
            reply_packet.protocol_type = 33
        # GetInfo
        elif command.protocol_type == 34:
            reply_payload.append(int(time.time()*1000000000))
            reply_payload.append(int(time.time()*1000000000) - self.on_time)
            reply_payload.append(0)             # Downtime not supported
            reply_packet.protocol_type = 35
        # GetLocation
        elif command.protocol_type == 48:
            reply_payload.append(self.location)
            reply_payload.append(self.location_label)
            reply_payload.append(self.location_updated_at)
            reply_packet.protocol_type = 50
        # GetGroup
        elif command.protocol_type == 51:
            reply_payload.append(self.group)
            reply_payload.append(self.group_label)
            reply_payload.append(self.group_updated_at)
            reply_packet.protocol_type = 53
        # EchoRequest
        elif command.protocol_type == 58:
            reply_payload.append(command_payload)
            reply_packet.protocol_type = 59
        # Get
        elif command.protocol_type == 101:
            reply_payload.append(self.color_hue)
            reply_payload.append(self.color_saturation)
            reply_payload.append(self.color_brightness)
            reply_payload.append(self.color_kelvin)
            reply_payload.append(0)
            reply_payload.append(self.level)
            reply_payload.append(self.label)
            reply_payload.append(0)
            reply_packet.protocol_type = 107
        # SetColor
        elif command.protocol_type == 102:
            self.color_hue = command_args[1]
            self.color_saturation = command_args[2]
            self.color_brightness = command_args[3]
            self.color_kelvin = command_args[4]
            if command.frame_address_res_required == 1:
                reply_payload.append(self.color_hue)
                reply_payload.append(self.color_saturation)
                reply_payload.append(self.color_brightness)
                reply_payload.append(self.color_kelvin)
                reply_payload.append(0)
                reply_payload.append(self.level)
                reply_payload.append(self.label)
                reply_payload.append(0)
                reply_packet.protocol_type = 107
        # GetPower
        elif command.protocol_type == 116:
            reply_payload.append(self.level)
            reply_packet.protocol_type = 118
        # SetPowerLight
        elif command.protocol_type == 117:
            self.level = command_args[0]
            if command.frame_address_res_required == 1:
                reply_payload.append(self.level)
                reply_packet.protocol_type = 118
        # GetInfrared
        elif command.protocol_type == 120:
            pass
        # SetInfrared
        elif command.protocol_type == 122:
            pass
        # Accounts for undocumented protocol types
        else:
            pass
  
        # If an ack is required, send that first
        if command.frame_address_ack_required == 1:
            # New LIFX packet for ACK
            temp_pack = l.LifxPacket()
            temp_pack.frame_address_target = self.mac_address.decode("hex") + \
                                            '\x00\x00'
            temp_pack.frame_address_sequence = command.frame_address_sequence
            temp_pack.frame_origin = 1
            # Reply source matches command source
            temp_pack.frame_source = command.frame_source
            reply_data.append(temp_pack.pack_send(0, 0, 'Ack', ))

        # Creates a tuple of reply payloads
        reply_payload = tuple(reply_payload)

        # Returns early if invalid protocol type, but CAN send ACK still
        if reply_packet.protocol_type == 0:
            return reply_data

        # Adds reply to list, will return all reply packets in one list
        reply_data.append(reply_packet.pack_send(0, 0, \
                    l.PROT_NAME[reply_packet.protocol_type], \
                    reply_payload))
    
        # Sends a second StateService message, not documented but observed
        if command.protocol_type == 2:
            temp_pld = []
            temp_pld.append(5)
            temp_pld.append(UDP_PORT)
            reply_data.append(reply_packet.pack_send(0, 0, \
                    l.PROT_NAME[reply_packet.protocol_type], \
                    temp_pld))
        
        # Returns list of all reply messages to be sent over socket
        return reply_data

