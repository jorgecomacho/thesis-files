#!/usr/bin/env python

__author__ = 'John Kalin'

"""listener.py: listens for LIFX traffic and prints its details in a readable
format
Author: John Kalin"""

import lifx_packet
import lifx_bulb
from socket import *
import threading

# UDP socket setup
UDP_IP = "0.0.0.0"
UDP_PORT = 56700

sock = socket(AF_INET, SOCK_DGRAM)   # INTERNET, UDP
sock.bind((UDP_IP, UDP_PORT))
sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
sock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

# Prints received packets
def listen_sock():
    while True:
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        print '\n\nRECEIVED: ', data, 'from', addr[0]
        response = lifx_packet.LifxPacket()
        payload = response.unpack_header(data)
        response.print_packet()   
        args = response.dissect_payload(payload)
        print response.protocol_type, 'payload', args

if __name__ == "__main__":

    while True:
        listen_sock()
      
