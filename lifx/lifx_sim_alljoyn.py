#!/usr/bin/env python

__author__ = 'John Kalin'

"""lifx_sim_alljoyn.py: This program sends out two alljoyn mdns packets every 
                        5 seconds to mimic LIFX bulb functionality"""

from socket import *
import time

UDP_IP = "0.0.0.0"
UDP_PORT = 1665

# Socket setup for packet transmission
sock = socket(AF_INET, SOCK_DGRAM)              # INTERNET, UDP
sock.bind((UDP_IP, UDP_PORT))
sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
sock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)


def send_sock():
    # Hex string with static alljoyn message
    data = '0df600000002000000000002085f616c6c6a6f796e045f7463700'
    data += '56c6f63616c00000c8001085f616c6c6a6f796e045f756470c01a'
    data += '000c800106736561726368203366316634616435343231613332'
    data += '383938303336353031623966316234623036c01a0010000100000078'
    data += '002709747874766572733d30186e5f313d6f72672e616c6c6a6f796e2e'
    data += '4275734e6f64652a036d3d310b73656e6465722d696e666fc04000100001'
    data += '00000078003e09747874766572733d3007616a70763d31300470763d320'
    data += '87369643d3335373411697076343d3139322e3136382e31302e310b757063'
    data += '76343d3634373936'

    # Converts to byte string
    tmp = data.decode("hex")  
    
    # Sends to brodcast and mdns addresses  
    sock.sendto(tmp, ('224.0.0.251', 5353))
    sock.sendto(tmp, ('255.255.255.255', 5353))

if __name__ == "__main__":

    # Sends advertisement every 5 seconds
    while True:
        send_sock()
        time.sleep(5)
      
