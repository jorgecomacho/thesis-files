#!/bin/bash

# Installation steps from http://docs.openhab.org/installation/linux.html#package-repository-installation

# Installs current version of Oracle Java
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer

# Adds Bintree repository key
wget -qO - 'https://bintray.com/user/downloadSubjectPublicKey?username=openhab' | sudo apt-key add -
sudo apt-get install apt-transport-https

# Adds stable OpenHAB to apt list
echo 'deb https://dl.bintray.com/openhab/apt-repo2 stable main' | sudo tee /etc/apt/sources.list.d/openhab2.list

sudo apt-get update

# Installs OpenHAB 2 and addons
sudo apt-get install openhab2
sudo apt-get install openhab2-addons

# Starts OpenHAB service
sudo systemctl start openhab2.service
sudo systemctl status openhab2.service

sudo systemctl daemon-reload
sudo systemctl enable openhab2.service
